# InteropTemplate

Very basic PInvoke Template for using Native cplusplus library in VL. As an intermediate Layer a Managed C# Wrapper is used. 
It shows wrapping of float and string. 
To return string values it makes use of BSTR as a Marshelling Type.
A BSTR is a composite data type that consists of a length prefix, a data string, and a terminator. 

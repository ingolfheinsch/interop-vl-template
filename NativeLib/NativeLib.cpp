#include "NativeLib.h"

// convert const char to BSTR
BSTR ANSItoBSTR(const char* input)
{
	BSTR result = NULL;
	int lenA = lstrlenA(input);
	int lenW = ::MultiByteToWideChar(CP_ACP, 0, input, lenA, NULL, 0);
	if (lenW > 0)
	{
		result = ::SysAllocStringLen(0, lenW);
		::MultiByteToWideChar(CP_ACP, 0, input, lenA, result, lenW);
	}
	return result;
}

NATIVE_API const OLECHAR* _String(const char* str) {
	return ::SysAllocString(ANSItoBSTR(str));
}

NATIVE_API float _Float(float value) {
		return value;
}
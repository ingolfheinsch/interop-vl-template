﻿using System.Runtime.InteropServices;

namespace Invoke
{
    public static class InvokeLayer
    {
        [DllImport("NativeLib.dll", EntryPoint = "_String")]
        [return: MarshalAs(UnmanagedType.BStr)]
        public static extern string InterString(string str);

        [DllImport("NativeLib.dll", EntryPoint = "_Float")]
        public static extern float InterFloat(float value);
    }
}